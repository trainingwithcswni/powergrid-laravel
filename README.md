# Powergrid con Laravel 8

Paso a paso para la integración del plugin de tablas dinámicas para Laravel 8

## Introducción

Este documento es creado para el apoyo en la integración básica del plugin que permite la visualización de datos en tablas interactivas

## Autor
Equipo de Training CSWNI

## Creador

| Usuario        | Nombres                    |
|----------------|----------------------------|
| @cswni         | Carlos Andres Perez Ubeda  |

## Licencia
Apache License, Version 2.0

