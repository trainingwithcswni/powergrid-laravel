# Project information
site_name: Integración de PowerGrid en Laravel 8
site_url: https://trainingwithcswni.gitlab.io/powergrid-laravel
site_author: Team CSWNI
site_description: >-
  Manual para la integración de PowerGrid en Laravel. Uso de Jetstream para facilitar las dependencias requeridas.

# Repository
repo_name: trainingwithcswni/powergrid-laravel
repo_url: https://gitlab.com/trainingwithcswni/powergrid-laravel
edit_uri: https://gitlab.com/trainingwithcswni/powergrid-laravel

# Copyright
copyright: Copyright &copy 2022 Team CSWNI

# Configuration
theme:
  name: null
  custom_dir: !ENV [ THEME_DIR, "material" ]

  # Static files
  static_templates:
    - 404.html

  # Don't include MkDocs' JavaScript
  include_search_page: false
  search_index_only: true

  # Default values, taken from mkdocs_theme.yml
  language: es
  features:
    - content.code.annotate
    # - content.tabs.link
    # - header.autohide
    - navigation.expand
    - navigation.indexes
    # - navigation.instant
    - navigation.sections
    - navigation.tabs
    # - navigation.tabs.sticky
    - navigation.top
    - navigation.tracking
    - search.highlight
    - search.share
    - search.suggest
    - toc.follow
    # - toc.integrate
  palette:
    - scheme: default
      primary: blue
      accent: orange
      toggle:
        icon: material/toggle-switch
        name: Modo oscuro
    - scheme: slate
      primary: red
      accent: red
      toggle:
        icon: material/toggle-switch-off-outline
        name: Modo oscuro
  font:
    text: Roboto
    code: Roboto Mono
  favicon: assets/favicon.png
  icon:
    logo: material/laravel

# Plugins
plugins:
  - search
  - minify:
      minify_html: true

# Customization
extra:
  analytics:
    provider: google
    property: !ENV GOOGLE_ANALYTICS_KEY
  social:
    - icon: fontawesome/brands/github
      link: https://github.com/trainingwithcswni/powergrid-laravel
    - icon: fontawesome/brands/gitlab
      link: https://gitlab.com/trainingwithcswni/powergrid-laravel

# Extensions
markdown_extensions:
  - abbr
  - admonition
  - attr_list
  - def_list
  - footnotes
  - meta
  - md_in_html
  - toc:
      permalink: true
  - pymdownx.arithmatex:
      generic: true
  - pymdownx.betterem:
      smart_enable: all
  - pymdownx.caret
  - pymdownx.details
  - pymdownx.emoji:
      emoji_generator: !!python/name:materialx.emoji.to_svg
      emoji_index: !!python/name:materialx.emoji.twemoji
  - pymdownx.highlight:
      anchor_linenums: true
  - pymdownx.inlinehilite
  - pymdownx.keys
  - pymdownx.magiclink:
      repo_url_shorthand: true
      user: squidfunk
      repo: mkdocs-material
  - pymdownx.mark
  - pymdownx.smartsymbols
  - pymdownx.superfences:
      custom_fences:
        - name: mermaid
          class: mermaid
          format: !!python/name:pymdownx.superfences.fence_code_format
  - pymdownx.tabbed:
      alternate_style: true
  - pymdownx.tasklist:
      custom_checkbox: true
  - pymdownx.tilde

# Page tree
nav:
  - Inicio: index.md
  - Requerimientos:
      - Programas necesarios: requerimientos.md
      - Validación de versiones: versiones.md
      - Ajustes para Git local: config.md
      - Instalar / actualizar Laravel installer: laravel-installer.md
      - Crear llave SSH: llave.md
      - Añadir llave:
          - A GitHub: agregar-llave-hub.md
          - A GitLab: agregar-llave-lab.md
  - Paso a paso:
      - Instalar laravel: paso1.md
      - Subir a repositorios en linea:
          - A GitLab: paso2.md
          - A Github: paso3.md
      - Instalar dependencias javascript: npm.md
      - Compilar el proyecto: build.md
      - Crear la base de datos: phpmyadmin.md
      - Abrir el proyecto en VSCode: code.md
      - Modificar el proveedor de servicios: provider.md
      - Configuración de entorno: env.md
      - Ejecutar migraciones inciales: migrate-1.md
      - Ingresar al proyecto: server.md
      - Crear modelos de ejemplo:
          - Proyecto : proyecto.md
          - Actividad : actividades.md
      - Modificar las migraciones:
          - Proyectos: tabla-proyecto.md
          - Activdiades: tabla-actividades.md
      - Crear datos de pruebas:
          - Proyectos: seeder-proyecto.md
          - Actividades: seeder-actividades.md
      - Configurar el DatabaseSedeer: seeder.md
      - Ejecutar migraciones: migrate-2.md
      - Componentes Livewire:
          - Proyecto: livewire-proyecto.md
          - Activdiades: livewire-actividades.md
      - Rutas de acceso: rutas.md
      - Tablas PowerGrid:
          - Proyecto: grid-proyectos.md
          - Actividades: grid-actividades.md
      - Integrar tablas en componentes Livewire: integracion.md
      - Recomendaciones: recomendaciones.md
